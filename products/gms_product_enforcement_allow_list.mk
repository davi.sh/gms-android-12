PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GoogleExtShared/oat/% \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/GooglePackageInstaller/oat/% \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/app/GooglePrintRecommendationService/oat/% \
    system/priv-app/TagGoogle/TagGoogle.apk \
    system/priv-app/TagGoogle/oat/% \
    system/etc/permissions/privapp-permissions-google-system.xml \
    system/etc/sysconfig/google-hiddenapi-package-allowlist.xml \

