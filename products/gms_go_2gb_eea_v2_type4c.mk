$(call inherit-product, vendor/partner_gms/products/eea_go_common.mk)

PRODUCT_PACKAGES += \
    sysconfig_eea_v2_search_chrome \
    GmsConfigOverlaySearchGo \
    AssistantGo \
    GoogleSearchGo \
    Chrome \
    SearchSelector \
    GmsEEAType4cIntegrationGo_2GB \
    Launcher3Go \
    Drive \
    Videos \
    YTMusic \
    Gmail2
